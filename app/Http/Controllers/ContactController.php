<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\ContactMethod;
use Illuminate\Http\Request;

/**
 * Class ContactController
 */
class ContactController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $contact = Contact::create($request->except(['methods']));

        $contactMethod = ContactMethod::make($request->input('methods')[0]);

        $contact->methods()->save($contactMethod);
    }
}
