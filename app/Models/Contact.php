<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Contact
 *
 * @property int                                                                  $id
 * @property string                                                               $first_name
 * @property string                                                               $last_name
 * @property \Carbon\Carbon                                                       $created_at
 * @property \Carbon\Carbon                                                       $updated_at
 * @property \App\Models\ContactMethod[]|\Illuminate\Database\Eloquent\Collection $methods
 * @property string                                                               $last_first
 */
class Contact extends Model
{
    /** @var array */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    /**
     * @return string
     */
    public function getLastFirstAttribute()
    {
        return $this->last_name . ', ' . $this->first_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function methods()
    {
        return $this->hasMany(ContactMethod::class);
    }
}
