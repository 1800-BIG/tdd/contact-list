<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactMethod
 *
 * @property int            $id
 * @property int            $contact_id
 * @property string         $type
 * @property string         $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ContactMethod extends Model
{
    /** @var array */
    protected $fillable = [
        'type',
        'value',
    ];
}
