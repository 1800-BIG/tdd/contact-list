<?php

namespace Tests\Feature;

use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class ContactTest
 */
class ContactTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function can_create_contact_with_method()
    {
        $this->withoutExceptionHandling();
        $postData = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'methods' => [
                [
                    'type' => 'mobile',
                    'value' => '8018675309',
                ],
            ],
        ];

        $response = $this->post(route('contacts.store'), $postData);

        $response->assertStatus(Response::HTTP_OK);
        $contact = Contact::where('first_name', $postData['first_name'])
            ->where('last_name', $postData['last_name'])
            ->first();
        $this->assertNotEmpty($contact);
        $this->assertCount(1, $contact->methods);
    }
}
