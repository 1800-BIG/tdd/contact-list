<?php

namespace Tests\Unit\Models;

use App\Models\Contact;
use Tests\TestCase;

/**
 * Class ContactTest
 */
class ContactTest extends TestCase
{
    /** @test */
    public function it_displays_last_comma_first()
    {
        $contact = factory(Contact::class)->make();

        $this->assertEquals($contact->last_name . ', ' . $contact->first_name, $contact->last_first);
    }
}
