<?php

/** @var \Illuminate\Routing\Router $router */
$router->post('contacts', 'ContactController@store')->name('contacts.store');

Route::get('/',
    function () {
        return view('welcome');
    });
