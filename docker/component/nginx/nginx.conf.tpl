user nginx;
pid /var/run/nginx.pid;
error_log /var/log/nginx/error.log warn;
pcre_jit on;
worker_processes auto;

events {
    worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    server_tokens off;

    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log  main;

    sendfile on;
    tcp_nodelay on;
    ssl_prefer_server_ciphers on;
    keepalive_timeout  65;

    gzip on;
    gzip_vary on;

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /app/public;
        server_name ${APP_DOMAIN};
        index index.php;
        #client_max_body_size 10M;

        #error_page  404              /404.html;
        #error_page   500 502 503 504  /50x.html;
        #location = /50x.html {
        #    root   /usr/share/nginx/html;
        #}

        location /- {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*' 'always';
                add_header 'Access-Control-Allow-Headers' 'x-requested-with' 'always';
                add_header 'Access-Cotnrol-Max-Age' 1296000 'always';
                add_header 'Content-Length' 0 'always';
                return 204;
            }

            add_header 'Cache-Control' 'public';
            expires +1y;

            try_files $uri =404;
        }

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
            fastcgi_pass ${PHP_FPM_HOST}:9000;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }
}
