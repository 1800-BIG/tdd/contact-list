BUILD_VERSION ?= latest

.PHONY: default

.DEFAULT:
	@echo
	@echo 'Invalid target.'
	@echo
	@echo '    local          build local images and start containers'
	@echo '    local-xdebug   run local images with xdebug support'
	@echo '    local-down     tear down local images'
	@echo '    local-reset    run local-down && make local'
	@echo '    local-xreset   run local-down && make local-xdebug'
	@echo '    test           build test images and run test suites'
	@echo

default: .DEFAULT


# components
fpm:
	docker build --pull \
		. -f docker/component/fpm/Dockerfile \
		-t tdd-example:fpm-$(BUILD_VERSION)

nginx:
	docker build --pull \
		. -f docker/component/nginx/Dockerfile \
		-t tdd-example:nginx-$(BUILD_VERSION)


# stages
local-base: fpm nginx
	docker build \
		. -f docker/env/local/Dockerfile.fpm \
		-t tdd-example:fpm-local

local: local-base
	docker-compose up -d

local-xdebug: local-base
	XDEBUG=true docker-compose up -d

local-down:
	docker-compose down -v

local-reset: local-down local

local-xreset: local-down local-xdebug


test: fpm
	docker build \
		--build-arg BUILD_VERSION=$(BUILD_VERSION) \
		. -f docker/env/testing/Dockerfile.fpm \
		-t tdd-example:test-fpm-$(BUILD_VERSION)
	BUILD_VERSION=$(BUILD_VERSION) docker-compose \
		-f docker/env/testing/docker-compose.yml run --rm tdd-example-app \
		sh -c "phpunit"
	make test-down

test-down:
	BUILD_VERSION=$(BUILD_VERSION) docker-compose \
		-f docker/env/testing/docker-compose.yml down -v
